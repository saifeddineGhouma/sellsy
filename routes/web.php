<?php

Route::get('/', 'HomeController@index')->name('home');

Route::get('/pages/{page}', 'PagesController@show')->where(['page' => '[0-9a-z-]+']);

Auth::routes();

Route::match(['get', 'post'], 'register', function () {
	return redirect()->route('login');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::get('permissions/getData', 'PermissionsController@getData')->name('permissions.getData');
    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::get('roles/getData', 'RolesController@getData')->name('roles.getData');
    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::get('users/getData', 'UsersController@getData')->name('users.getData');
    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');
    Route::resource('products', 'ProductsController');

    // Pages
    Route::delete('pages/destroy', 'PagesController@massDestroy')->name('pages.massDestroy');
    Route::get('pages/getData', 'PagesController@getData')->name('pages.getData');
    Route::resource('/pages', 'PagesController');

    Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });

    // Learnybox
    Route::get('learnybox/index', 'LearnyboxController@index')->name('learnybox.index');
    Route::get('learnybox/getData', 'LearnyboxController@getData')->name('learnybox.getData');
   
    // ADA
    Route::get('ada/index', 'AdaController@index')->name('ada.index');
    Route::get('ada/getData', 'AdaController@getData')->name('ada.getData');
});
