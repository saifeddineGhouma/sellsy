<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';

    public $incrementing = false;

    public $timestamps = false;

    protected $primaryKey = 'lb';

    protected $fillable = [
        'lb',
        'sellsy',
        'mnt_sellsy',
        'mnt_lb',
        'status',
        'id_trans',
        'nom',
        'prenom',
        'email',
    ];
}
