<?php

namespace App\Http\Requests;

use App\Page;
use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('page_create');
    }

    public function rules()
    {
        return [
            'titre' => 'required|unique:pages,titre|max:255',
            'template_name' => 'required'
        ];
    }
}
