<?php

namespace App\Http\Requests;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePageRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('page_edit');
    }

    public function rules(Request $request)
    {
        $page = Page::find($request->segment(3));

        return [
            'titre' => 'required|max:255|unique:pages,titre,' . $page->id,
            'template_name' => 'required',
        ];
    }
}
