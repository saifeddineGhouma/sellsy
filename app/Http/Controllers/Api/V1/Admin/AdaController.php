<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdaController extends Controller
{
    public function validateDoc($id_order) {
        $order = Order::where('id_order', $id_order)->first();
        $order->status = true;
        $order->save();
        return response()->json();
    }
}
