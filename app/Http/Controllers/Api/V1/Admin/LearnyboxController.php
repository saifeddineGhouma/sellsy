<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LearnyboxController extends Controller
{
    public function validateDoc($lb) {
        $document = Document::where('lb', $lb)->first();
        $document->status = true;
        $document->save();
        return response()->json();
    }
}
