<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class AdaController extends Controller
{
    public function index()
    {
        return view('admin.ada.index');
    }

    public function getData()
    {
        $query = Order::query()->where('id_shop', 2)->whereNotNull('sellsy_document');

        $columns = DataTables::of($query);

        $columns->editColumn('status', function (Order $row) {
            switch ($row->status) {
                case 1:
                    return '<span class="text-success">OK</span>';
                    break;
                case 0:
                    return '<span class="text-danger">KO</span> <button onclick="valider(\''.route('admin.ada.validate', $row->id_order).'\')" class="btn btn-xs btn-primary" title="valider">valider</button>';
                    break;
                default:
                    return '<span class="text-danger">En attente</span> <button onclick="valider(\''.route('admin.ada.validate', $row->id_order).'\')" class="btn btn-xs btn-primary" title="valider">valider</button>';
                    break;
            }
        })->rawColumns(['status'])
        ->escapeColumns(['status']);

        return $columns->make(true);
    }
}
