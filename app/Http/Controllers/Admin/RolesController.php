<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\Permission;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\MassDestroyRoleRequest;

class RolesController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('role_access'), 403);

        return view('admin.roles.index');
    }

    public function getData()
    {
        $query = Role::query();
        
        $columns = DataTables::of($query);

        $columns->with('permissions');

        $columns
            ->editColumn('title', function (Role $row) {
                return $row->title ?? '';
            })
            
            ->addColumn('permissions', function (Role $row) {
                $permissions = '';
                foreach($row->permissions as $key => $item) {
                    $permissions .= '<span class="badge badge-info">' . $item->title . '</span> ';
                }
                return $permissions;
            })

            ->addColumn('actions', function ($row) {
                $actions = '';
                
                if (\Gate::allows('role_show')) {
                    $actions .= '<a class="btn btn-xs btn-primary" href="' . route('admin.roles.show', $row->id) . '" title="' . trans("global.view") . '">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a> ';
                }
                if (\Gate::allows('role_edit')) {
                    $actions .= '<a class="btn btn-xs btn-info" href="' . route('admin.roles.edit', $row->id) . '" title="' . trans('global.edit') . '">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a> ';
                }
                if (\Gate::allows('role_delete')) {
                    $actions .= '<form action="' . route('admin.roles.destroy', $row->id) . '" method="POST" onsubmit="ConfirmDelete()" style="display: inline-block;">
                        <input name="_method" value="DELETE" type="hidden">
                        ' . csrf_field() . '
                        <button type="submit" class="btn btn-xs btn-danger" title="' . trans('global.delete') . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </form>';
                }

                return $actions;
            })
            ->rawColumns(['title', 'permissions', 'actions'])
            ->escapeColumns(['actions']);

        return $columns->make(true);
    }

    public function create()
    {
        abort_unless(\Gate::allows('role_create'), 403);

        $permissions = Permission::all()->pluck('title', 'id');

        return view('admin.roles.create', compact('permissions'));
    }

    public function store(StoreRoleRequest $request)
    {
        abort_unless(\Gate::allows('role_create'), 403);

        $role = Role::create($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('admin.roles.index');
    }

    public function edit(Role $role)
    {
        abort_unless(\Gate::allows('role_edit'), 403);

        $permissions = Permission::all()->pluck('title', 'id');

        $role->load('permissions');

        return view('admin.roles.edit', compact('permissions', 'role'));
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        abort_unless(\Gate::allows('role_edit'), 403);

        $role->update($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('admin.roles.index');
    }

    public function show(Role $role)
    {
        abort_unless(\Gate::allows('role_show'), 403);

        $role->load('permissions');

        return view('admin.roles.show', compact('role'));
    }

    public function destroy(Role $role)
    {
        abort_unless(\Gate::allows('role_delete'), 403);

        $role->delete();

        return back();
    }

    public function massDestroy(MassDestroyRoleRequest $request)
    {
        Role::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
