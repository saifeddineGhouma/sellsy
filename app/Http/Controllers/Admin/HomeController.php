<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ok = Document::where('status', 1)->get()->count();
        $ko = Document::where('status', 0)->get()->count();
        $attente = Document::whereNull('status')->get()->count();

        $ada_ok = Order::where('status', 1)->get()->count();
        $ada_ko = Order::where('status', 0)->get()->count();
        $ada_attente = Order::whereNull('status')->get()->count();


        return view('admin.home', compact('ok', 'ko', 'attente', 'ada_ok', 'ada_ko', 'ada_attente'));
    }
}
