<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePageRequest;
use App\Http\Requests\UpdatePageRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\MassDestroyPageRequest;

class PagesController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('page_access'), 403);

        return view('admin.pages.index');
    }

    public function getData()
    {
        $query = Page::query();
        
        $columns = DataTables::of($query);

        $columns
            ->editColumn('titre', function (Page $row) {
                return $row->titre ?? '';
            })
            ->editColumn('slug', function (Page $row) {
                return $row->slug ?? '';
            })
            ->editColumn('template_name', function (Page $row) {
                return ($row->template_name == 1) ? 'Simple colonne' : 'Double colonnes';
            })
            ->editColumn('active', function (Page $row) {
                return ($row->active == true) ? 'oui' : 'non';
            })

            ->addColumn('actions', function ($row) {
                $actions = '';

                if (\Gate::allows('page_show')) {
                    $actions .= '<a class="btn btn-xs btn-primary" href="' . url('/') . '/pages/' . $row->slug . '" target="_blank" title="' . trans('global.view') . '">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a> ';
                }
                if (\Gate::allows('page_edit')) {
                    $actions .= '<a class="btn btn-xs btn-info" href="' . route('admin.pages.edit', $row->id) . '" title="' . trans('global.edit') . '">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a> ';
                }
                if (\Gate::allows('page_delete')) {
                    $actions .= '<form action="' . route('admin.pages.destroy', $row->id) . '" method="POST" onsubmit="ConfirmDelete()" style="display: inline-block;">
                        <input name="_method" value="DELETE" type="hidden">
                        ' . csrf_field() . '
                        <button type="submit" class="btn btn-xs btn-danger" title="' . trans('global.delete') . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </form>';
                }

                return $actions;
            })
            ->rawColumns(['titre', 'slug', 'template_name', 'active', 'actions'])
            ->escapeColumns(['actions']);

        return $columns->make(true);
    }

    public function create()
    {
        abort_unless(\Gate::allows('page_create'), 403);

        return view('admin.pages.create');
    }

    public function store(StorePageRequest $request)
    {
        abort_unless(\Gate::allows('page_create'), 403);
        if (isset($request->active))
            $request->merge(['active' => true]);
        else
            $request->merge(['active' => false]);
            
        $request->merge(['slug' => (!is_null($request->slug) ? Str::slug($request->slug, '-') : Str::slug($request->titre, '-'))]);
        $request->merge(['template_name' => intval($request->template_name)]);
        $page = Page::create($request->all());

        return redirect()->route('admin.pages.index');
    }

    public function edit(Page $page)
    {
        abort_unless(\Gate::allows('page_edit'), 403);

        return view('admin.pages.edit', compact('page'));
    }

    public function update(UpdatePageRequest $request, Page $page)
    {
        abort_unless(\Gate::allows('page_edit'), 403);
		$request->merge(['slug' => (!is_null($request->slug) ? Str::slug($request->slug, '-') : Str::slug($request->titre, '-'))]);
        $request->merge(['template_name' => intval($request->template_name)]);
        if (isset($request->active))
            $request->merge(['active' => true]);
        else
            $request->merge(['active' => false]);

        $page->update($request->all());

        return redirect()->route('admin.pages.index');
    }

    public function destroy(Page $page)
    {
        abort_unless(\Gate::allows('page_delete'), 403);

        $page->delete();

        return back();
    }

    public function massDestroy(MassDestroyPageRequest $request)
    {
        Page::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
