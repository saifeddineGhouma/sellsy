<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\MassDestroyUserRequest;

class UsersController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('user_access'), 403);

        return view('admin.users.index');
    }

    public function getData()
    {
        $query = User::query();
        
        $columns = DataTables::of($query);

        $columns
            ->editColumn('name', function (User $row) {
                return $row->name ?? '';
            })
            ->editColumn('email', function (User $row) {
                return $row->email ?? '';
            })
            ->editColumn('email_verified_at', function (User $row) {
                return $row->email_verified_at ?? '';
            })
            ->addColumn('roles', function (User $row) {
                $roles = '';
                foreach($row->roles as $key => $item) {
                    $roles .= '<span class="badge badge-info">' . $item->title . '</span>';
                }
                return $roles;
            })
            
            ->addColumn('actions', function ($row) {
                $actions = '';
                
                if (\Gate::allows('user_show')) {
                    $actions .= '<a class="btn btn-xs btn-primary" href="' . route('admin.users.show', $row->id) . '" title="' . trans('global.view') . '">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a> ';
                }
                if (\Gate::allows('user_edit')) {
                    $actions .= '<a class="btn btn-xs btn-info" href="' . route('admin.users.edit', $row->id) . '" title="' . trans('global.edit') . '">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a> ';
                }
                if (\Gate::allows('user_delete')) {
                    $actions .= '<form action="' . route('admin.users.destroy', $row->id) . '" method="POST" onsubmit="ConfirmDelete()" style="display: inline-block;">
                        <input name="_method" value="DELETE" type="hidden">
                        ' . csrf_field() . '
                        <button type="submit" class="btn btn-xs btn-danger" title="' . trans('global.delete') . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </form>';
                }

                return $actions;
            })
            ->rawColumns(['name', 'email', 'email_verified_at', 'roles', 'actions'])
            ->escapeColumns(['actions']);

        return $columns->make(true);
    }

    public function create()
    {
        abort_unless(\Gate::allows('user_create'), 403);

        $roles = Role::all()->pluck('title', 'id');

        return view('admin.users.create', compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        abort_unless(\Gate::allows('user_create'), 403);

        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_unless(\Gate::allows('user_edit'), 403);

        $roles = Role::all()->pluck('title', 'id');

        $user->load('roles');

        return view('admin.users.edit', compact('roles', 'user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        abort_unless(\Gate::allows('user_edit'), 403);

        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_unless(\Gate::allows('user_show'), 403);

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_unless(\Gate::allows('user_delete'), 403);

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
