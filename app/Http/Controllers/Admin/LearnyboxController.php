<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class LearnyboxController extends Controller
{
    public function index()
    {
        return view('admin.learnybox.index');
    }

    public function getData()
    {
        $query = Document::query();

        $columns = DataTables::of($query);

        $columns->editColumn('status', function (Document $row) {
            switch ($row->status) {
                case 1:
                    return '<span class="text-success">OK</span>';
                    break;
                case 0:
                    return '<span class="text-danger">KO</span> <button onclick="valider(\''.route('admin.learnybox.validate', $row->lb).'\')" class="btn btn-xs btn-primary" title="valider">valider</button>';
                    break;
                default:
                    return '<span class="text-danger">En attente</span> <button onclick="valider(\''.route('admin.learnybox.validate', $row->lb).'\')" class="btn btn-xs btn-primary" title="valider">valider</button>';
                    break;
            }
        })->rawColumns(['status'])
        ->escapeColumns(['status']);

        return $columns->make(true);
    }
}
