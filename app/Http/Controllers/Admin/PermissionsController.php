<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StorePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use App\Http\Requests\MassDestroyPermissionRequest;

class PermissionsController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('permission_access'), 403);

        return view('admin.permissions.index');
    }

    public function getData()
    {
        $query = Permission::query();
        
        $columns = Datatables::of($query);

        $columns
            ->editColumn('title', function (Permission $query) {
                return $query->title ?? '';
            })
            ->addColumn('actions', function ($query) {
                $actions = '';

                if (\Gate::allows('permission_show')) {
                    $actions .= '<a class="btn btn-xs btn-primary" href="' . route("admin.permissions.show", $query->id) . '" title="' . trans("global.view") . '">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a> ';
                }
                if (\Gate::allows('permission_edit')) {
                    $actions .= '<a class="btn btn-xs btn-info" href="' . route("admin.permissions.edit", $query->id) . '" title="' . trans("global.edit") . '">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a> ';
                }
                if (\Gate::allows('permission_delete')) {
                    $actions .= '<form action="' . route('admin.permissions.destroy', $query->id) . '" method="POST" onsubmit="ConfirmDelete()" style="display: inline-block;">
                            <input name="_method" value="DELETE" type="hidden">
                                ' . csrf_field() . '
                            <button type="submit" class="btn btn-xs btn-danger" title="' . trans('global.delete') . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </form>';
                }

                return $actions;
            })
            ->rawColumns(['title', 'actions'])
            ->escapeColumns(['actions']);

        return $columns->make(true);
    }

    public function create()
    {
        abort_unless(\Gate::allows('permission_create'), 403);

        return view('admin.permissions.create');
    }

    public function store(StorePermissionRequest $request)
    {
        abort_unless(\Gate::allows('permission_create'), 403);

        $permission = Permission::create($request->all());

        return redirect()->route('admin.permissions.index');
    }

    public function edit(Permission $permission)
    {
        abort_unless(\Gate::allows('permission_edit'), 403);

        return view('admin.permissions.edit', compact('permission'));
    }

    public function update(UpdatePermissionRequest $request, Permission $permission)
    {
        abort_unless(\Gate::allows('permission_edit'), 403);

        $permission->update($request->all());

        return redirect()->route('admin.permissions.index');
    }

    public function show(Permission $permission)
    {
        abort_unless(\Gate::allows('permission_show'), 403);

        return view('admin.permissions.show', compact('permission'));
    }

    public function destroy(Permission $permission)
    {
        abort_unless(\Gate::allows('permission_delete'), 403);

        $permission->delete();

        return back();
    }

    public function massDestroy(MassDestroyPermissionRequest $request)
    {
        Permission::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
