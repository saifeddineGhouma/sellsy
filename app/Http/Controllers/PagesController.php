<?php

namespace App\Http\Controllers;

use App\Page;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Http\Requests\MassDestroyPageRequest;

class PagesController extends Controller
{
    public function show($slug)
	{
        $page = Page::where('slug', $slug)->first();

        if ($page == NULL)
            abort_unless(\Gate::allows('page_show'), 404);

        if ($page->template_name == 1)
            return view('fo.pages.simple', compact('page'));
        else
            return view('fo.pages.double', compact('page'));
    }
}
