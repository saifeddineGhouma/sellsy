<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\lbapi as LBApi;

class learnybox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'learnybox:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lbApi = new LBApi(env('LB_API_ENDPOINT'), env('LB_API_KEY'));
        $lbApi->set('action', 'transaction/get')->set('id_trans', '5ff8429bc9db');
        try {
            $call = $lbApi->call();
        } catch (Exception $e) {
            echo 'Une erreur est survenue : ' . $e->getMessage();
            exit();
        }
        print_r($call);

    }
}
