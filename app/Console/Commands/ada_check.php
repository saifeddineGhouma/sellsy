<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use App\Helpers\lbapi as LBApi;
use App\Helpers\sellsyTools as sellsyTools;
use App\Helpers\sellsyconnect as sellsyConnect;

class ada_check extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ada:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ADA check';

    /**
     * Get info from sellsy api with document id
     */
    private function getInfoSellsy($id){
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => $id 
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        try {
            return $response->response->totalAmount;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $documents = Order::whereNull('status')->where('id_shop', 2)->whereNotNull('sellsy_document')->get();
        foreach($documents as $document) {
            $this->info('Traitement en cours : '.$document->reference );
            //Get amount ps
            $mntPS = $document->total_paid_tax_incl;
    

            // Check data on Sellsy API
            $mntSellsy = $this->getInfoSellsy($document->sellsy_document);
            $document->montant_sellsy = round($mntSellsy,2);

            $document->status = null;
            if(round($mntSellsy,2) == round($mntPS,2)) {
                $document->status = 1;
            }
            if(round($mntSellsy,2) != round($mntPS,2)) {
                $document->status = 0;
            }
            $document->save();
        }
    }
}
