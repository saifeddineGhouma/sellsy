<?php

namespace App\Console\Commands;

use App\Document;
use Illuminate\Console\Command;
use App\Helpers\lbapi as LBApi;
use App\Helpers\sellsyTools as sellsyTools;
use App\Helpers\sellsyconnect as sellsyConnect;

class learnybox_check extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'learnybox:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Get info from sellsy api with document id
     */
    private function getInfoSellsy($id){
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => $id 
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        try {
            return $response->response->totalAmount;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Get info from LB api with id cart
     */
    private function getInfoLB($id){
        $lbApi = new LBApi(env('LB_API_ENDPOINT'), env('LB_API_KEY'));
        $lbApi->set('action', 'transaction/get')->set('id_trans', $id);
        $mntlb = 0;
        try {
            $call = $lbApi->call();
            foreach ($call['datas']['produits'] as $item) {
                $mntlb+= $item['montant_ttc'];
            }
        } catch (\Exception $e) {
            $this->error('Une erreur est survenue : ' . $e->getMessage());
        }
        
        return $mntlb;
        
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $documents = Document::whereNull('status')->orWhere('status', '!=', 1)->where('id_trans','!=','')->get();
        foreach($documents as $document) {
            $this->info('Traitement en cours : '.$document->id_trans );
            //Check data on LB API
            $mntLB = $this->getInfoLB($document->id_trans);
            $document->mnt_lb = $mntLB;

            // Check data on Sellsy API
            $mntSellsy = $this->getInfoSellsy($document->sellsy);
            $document->mnt_sellsy = $mntSellsy;

            $document->status = null;
            if(round($mntSellsy,2) == $mntLB) {
                $document->status = 1;
            }
            if(round($mntSellsy,2) != $mntLB) {
                $document->status = 0;
            }
            $document->save();
        }
    }
}
