<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\sellsyTools as sellsyTools;
use App\Helpers\sellsyconnect as sellsyConnect;

class sellsy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sellsy:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => 14088251
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        print_r($response);
    }
}
