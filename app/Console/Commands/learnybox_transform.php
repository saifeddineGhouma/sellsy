<?php

namespace App\Console\Commands;

use App\Document;
use \Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\lbapi as LBApi;
use App\Helpers\sellsyTools as sellsyTools;
use App\Helpers\sellsyconnect as sellsyConnect;

class learnybox_transform extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'learnybox:transform';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date_start = '2020-01-01';
        $date_end = Carbon::createFromFormat('Y-m-d', $date_start);
        $date_start = $date_end->format('Y-m-d');
        $date_end = $date_end->addDays(4)->format('Y-m-d');

        while ($date_end <= Carbon::now()->format('Y-m-d')) {
            $lbApi = new LBApi(env('LB_API_ENDPOINT'), env('LB_API_KEY'));
            $lbApi->set('action', 'transactions/get')->set('date_start', $date_start)->set('date_end', $date_end);
            try {
                $call = $lbApi->call();
            } catch (Exception $e) {
                echo 'Une erreur est survenue : ' . $e->getMessage();
                exit();
            }
            foreach ($call['datas']['transactions'] as $transaction) {
                //Search cart 
                $document = Document::where('lb',$transaction['id_cart'])->first();
    
                if($document)
                    $document->update([
                        'id_trans' => $transaction['id_trans'],
                        'nom' => $transaction['nom'],
                        'prenom' => $transaction['prenom'],
                        'email' => $transaction['email'],
                    ]);
            }

            $date_start = Carbon::createFromFormat('Y-m-d', $date_start);
            $date_end = Carbon::createFromFormat('Y-m-d', $date_end);
            $date_start = $date_start->addDays(4)->format('Y-m-d');
            $date_end = $date_end->addDays(4)->format('Y-m-d');
        }
    }
}
