<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Helpers\sellsyhelpers as sellsyhelpers;
use App\Helpers\sellsyconnect as sellsyConnect;

use DB ;
class prestashopToSellsy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sellsy:pstosellsy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create client On sellsy
     */
    /**
     * get tax
     */
    function getTaxeByValue($value) {
        $requestTax = array(
            'method' => 'Accountdatas.getTaxes',
            'params' => array(
                'includeEcoTax' => 'N',
                'enabled' => 'all',
            ),
        );
        $responseTax = sellsyConnect::load()->requestApi($requestTax);
        if (!empty($responseTax)) {
            foreach ($responseTax->response as $key => $taxe) {
                if (isset($taxe->value)) {
                    if (round($taxe->value,2) == $value) {
                        return $taxe;
                    }
                }
            }
        }
    
        return array();
    }
    /**
     * get taxt by id
     */
    function getTaxeById($id) {
        $request = array(
            'method' => 'Accountdatas.getTaxe',
            'params' => array(
                'id' => $id,
            ),
        );
        return sellsyConnect::load()->requestApi($request);
    }
    /**
     * create tax
     */
    function createTaxe($name,$taxe_value,$taxe_enabled){
        $request = array(
            'method' => 'Accountdatas.createTaxe',
            'params' => array(
                'taxe' => array(
                    'name'      => $name,
                    'value'     => $taxe_value,
                    'isEnabled' => $taxe_enabled
                )
            )
        );
        return sellsyConnect::load()->requestApi($request);
    }
    /**
     * 
     */
    private function getOrCreateCustomer($customer)
    {
        /**search client by email */
        $client_id = sellsyhelpers::SearchClientByEmail($customer->email);
         
                
        $name= sellsyhelpers::getName($customer);
        //* If not exist create client*/
        if(empty($client_id))
        {
            
            $name= sellsyhelpers::getName($customer);
            //**create client */            
            $client_id=sellsyhelpers::createClient($customer,$name);
            
        }
        return $client_id ;
    }
    private function createDocument($client_id,$order)
    {
        $request = array(
            'method' => 'Document.create',
            'params' => Array(
                'document' => Array(
                    'doctype' => 'invoice',
                    'thirdid' => $client_id,
                    'rateCategory' => 145236, // Mettre variable .env
                    'displayedDate' => date('U', strtotime($order->date_add)), //Recuperer la date sellsy
                ),
            ),
        );
    
        //*Construction des items*/
        $items=sellsyhelpers::getOrderDetail($order->id_order);
        $i = 1; 

        foreach ($items as $item)
        {
            //* calcul de la tva (Récuperer le code TVA dans prestashop)*/
        
            $tva = sellsyhelpers::getTVA($item);
            //* get tva from sellsy */
            $taxe = $this->getTaxeByValue(round($tva,2));
            ////*if taxe null///
            if (empty($taxe)) {
                $taxe = $this->createTaxe('TVA', $tva, 'Y');
                $taxe = $this->getTaxeById($taxe->response->taxeid);
                $taxe = $taxe->response;
            }
            $request['params']['row'][$i] = array(
                'row_type' => 'once',
                'row_name' => $item->product_name,
                'row_notes' => $item->product_name,
                'row_unit' => 'unité',
                'row_unitAmount' => $item->unit_price_tax_incl, //prix unitaire
                'row_taxid' => $taxe->id, //Ereur remonté au suppport 0 ne fonctionne pas à remplacer $tva
                'row_qt' => $item->product_quantity,
                'row_isOption' => 'N',
            );
        
            $i++;

        }
   
        //Enregistrer le produit
        if($order->total_shipping_tax_incl != 0) {
            $tva = $order->carrier_tax_rate ;					
            // get tva from sellsy
            $taxe = $this->getTaxeByValue(round($tva,2));
        
            $request['params']['row'][$i] = array(
                'row_type' => 'once',
                'row_name' => 'Frais de livraison',
                'row_notes' => 'Frais de livraison',
                'row_unit' => 'unité',
                'row_unitAmount' => $order->total_shipping_tax_incl, //prix unitaire
                'row_taxid' => $taxe->id, //Ereur remonté au suppport 0 ne fonctionne pas à remplacer $tva
                'row_qt' => 1,
                'row_isOption' => 'N',
            );
            $i++;
        }
   
        $request['params']['row'][$i] = array('row_type' => 'sum');       
        /**create document */
        $response = sellsyConnect::load()->requestApi($request);                       
        $idDocument = $response->response->doc_id;   

        return  $idDocument ;     
    }
    private function createPaiments($id_order_ref,$id_shop,$idDocument)
    {
        $paiements =sellsyhelpers::getPaiements($id_order_ref) ;
       
        if(!empty($paiements))
        {
          
            $payment_method = $paiements->payment_method;
           
            $MethodPayment = sellsyhelpers::getMethodPayment($id_shop,$payment_method);
            
            if(empty($MethodPayment))
            {
                $requestcreatePayMedium = array(
					'method' =>  'Accountdatas.createPayMedium',
					'params' => array(
						'paymedium' => array(
							'isEnabled' => 'Y',
							'value' => $payment_method
						)
					)
                );
                $rescreatePayMedium = sellsyConnect::load()->requestApi($requestcreatePayMedium);          
                $MethodPayment = $rescreatePayMedium->response->mediumid;
                /**create method payment */

            }else{
               
				$MethodPayment = $MethodPayment->id;
            }
            
            $request = array(
				'method' => 'Document.createPayment',
				'params' => array(
					'payment' => array(
						'date' => date('U', strtotime($paiements->date_add)),
						'amount' => $paiements->amount,
						'medium' => $MethodPayment, //Id du moyen de paiement (Paypal :  ; 3094196 : Virement ; 3094198 : Chèque )
						'doctype' => "invoice",
						'docid' => $idDocument,
						'ident' =>  $id_order_ref,
					),
				),
            );
            

            $response = sellsyConnect::load()->requestApi($request);
            $id_trans_sellsy = $response->response->payid;
            
            sellsyhelpers::UpdateOrderPayment($id_trans_sellsy,$id_order_ref);
            
        }

    }
    private function getinfo($id)
    {
        echo 'le client avec  id '.$id ;
    }
   
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      //Step 1 : Get orders
      $orders= sellsyhelpers::getOrders();

      //Step 2 : Treat Order by Order

      foreach($orders as $order) {

        //Step 4 :Get custommer information */
        $id_customer = $order->id_customer;
        $id_address_invoice = $order->id_address_invoice;
        $id_order = $order->id_order;
        $id_order_ref = $order->reference;
        $customer=sellsyhelpers::getCustomer($id_customer,$id_address_invoice);
            //* If infos exist on prestashop ==> Continue else End */
            if(!empty($customer)) {
               
                $client_id=$this->getOrCreateCustomer($customer);
                
                $idDocument= $this->createDocument($client_id,$order);
                //******update order */
                $query= sellsyhelpers::updateOrder($idDocument,$id_order); 

                //*paiements*/
                $id_shop = 2;
                $this->createPaiments($id_order_ref,$id_shop,$idDocument);
               

            
            }

      }
      

    }
}
