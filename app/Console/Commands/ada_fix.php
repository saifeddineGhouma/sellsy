<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use App\Helpers\lbapi as LBApi;
use App\Helpers\sellsyTools as sellsyTools;
use App\Helpers\sellsyconnect as sellsyConnect;
use DB;

class ada_fix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ada:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ADA fix probleme on document';

    /**
     * Get info from sellsy api with document id
     */
    private function getInfoSellsy($id){
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => $id 
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        try {
            return $response->response->totalAmount;
        } catch (\Throwable $th) {
            return null;
        }
    }

      /**
     * Get info from sellsy api with document id
     */
    private function getDocument($id){
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => $id 
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        try {
            return $response->response;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * get Tva Id from sellsy
     */
    function getTaxeByValue($value) {
        $request = array(
            'method' => 'Accountdatas.getTaxes',
            'params' => array(
                'includeEcoTax' => 'N',
                'enabled' => 'all',
            ),
        );
        $response = sellsyConnect::load()->requestApi($request);;
        if (!empty($response)) {
            foreach ($response->response as $key => $taxe) {
                if (isset($taxe->value)) {
                    if (round($taxe->value,2) == $value) {
                        return $taxe;
                    }
                }
            }
        }
        return array();
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $documents = Order::where('status',0)->where('id_shop', 2)->whereNotNull('sellsy_document')->get();
        //$documents = Order::where('sellsy_document',19874499)->get(); 
        foreach($documents as $document) {
            $this->info('Traitement en cours : '.$document->reference );
            //Get amount ps
            $mntPS = $document->total_paid_tax_incl;
    

            // Check data on Sellsy API
            $mntSellsy = $this->getInfoSellsy($document->sellsy_document);
            $document->montant_sellsy = round($mntSellsy,2);

            $document->status = null;
            $document->save();
            $DocumentSellsy = $this->getDocument($document->sellsy_document);

            //correspondance des offres
            $request = array(
                'method' => 'Document.update',
                'params' => Array(
                    'document' => Array(
                        'doctype' => 'invoice'
                    ),
                'docid' => $DocumentSellsy->id,
                ),
            );
            
            //Récupérarion des articles
            $i = 1;
            $items = DB::select('SELECT * FROM `ps_order_detail` where id_order = ?', [$document->id_order]);

            foreach ($items as $item) {
                $diff = $item->unit_price_tax_incl - $item->unit_price_tax_excl;
                if($diff != 0){
                    $tva = ( ($item->unit_price_tax_incl - $item->unit_price_tax_excl) / $item->unit_price_tax_excl) *100 ;
                }
                $taxe = $this->getTaxeByValue(round($tva,2));

                $request['params']['row'][$i] = array(
                    'row_type' => 'once',
                    'row_name' => $item->product_name,
                    'row_notes' => $item->product_name,
                    'row_unit' => 'unité',
                    'row_unitAmount' => $item->unit_price_tax_incl, //prix unitaire
                    'row_taxid' => $taxe->id, //Ereur remonté au suppport 0 ne fonctionne pas à remplacer $tva
                    'row_qt' => $item->product_quantity,
                    'row_isOption' => 'N',
                );
                $i++;
                
            } /* End froreach for items */

        

        //Shipping
        if($document->total_shipping_tax_incl != 0) {
			$tva = $document->carrier_tax_rate ;
					
			// get tva from sellsy
			$taxe = $this->getTaxeByValue(round($tva,2));
	

			$request['params']['row'][$i] = array(
				'row_type' => 'once',
				'row_name' => "Frais de livraison",
				'row_notes' => "Frais de livraison",
				'row_unit' => 'unité',
				'row_unitAmount' => $document->total_shipping_tax_incl, //prix unitaire
				'row_taxid' => $taxe->id, //Ereur remonté au suppport 0 ne fonctionne pas à remplacer $tva
				'row_qt' => 1,
				'row_isOption' => 'N',
			);
			$i++;
        } /* End shipping */
        
        $request['params']['row'][$i] = array('row_type' => 'sum');
        $response = sellsyConnect::load()->requestApi($request);
        $this->info('Traitement Status  : '.$response->status );

    } /* End foreach Document */
}
}
