<?php

namespace App\Console\Commands;

use App\Order;
use App\Document;
use Illuminate\Console\Command;
use App\Helpers\lbapi as LBApi;
use App\Helpers\sellsyTools as sellsyTools;
use App\Helpers\sellsyconnect as sellsyConnect;
use DB;

class lb_fix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lb:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ADA fix probleme on document';


    private function getInfoSellsy($id){
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => $id 
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        try {
            return $response->response->totalAmount;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * Get info from LB api with id cart
     */
    private function getInfoLB($id){
        $lbApi = new LBApi(env('LB_API_ENDPOINT'), env('LB_API_KEY'));
        $lbApi->set('action', 'transaction/get')->set('id_trans', $id);
        try {
            $call = $lbApi->call();
        } catch (\Exception $e) {
            $this->error('Une erreur est survenue : ' . $e->getMessage());
        }
        
        return $call;
        
    }

      /**
     * Get info from sellsy api with document id
     */
    private function getDocument($id){
        $request = array(
            'method' => 'Document.getOne',
            'params' => array(
                'doctype'   => 'invoice',
                'docid'     => $id 
            )
        );
        $response = sellsyConnect::load()->requestApi($request);
        try {
            return $response->response;
        } catch (\Throwable $th) {
            return null;
        }
    }

    /**
     * get Tva Id from sellsy
     */
    function getTaxeByValue($value) {
        $request = array(
            'method' => 'Accountdatas.getTaxes',
            'params' => array(
                'includeEcoTax' => 'N',
                'enabled' => 'all',
            ),
        );
        $response = sellsyConnect::load()->requestApi($request);;
        if (!empty($response)) {
            foreach ($response->response as $key => $taxe) {
                if (isset($taxe->value)) {
                    if (round($taxe->value,2) == $value) {
                        return $taxe;
                    }
                }
            }
        }
        return array();
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$documents = Document::where('status',0)->where('id_trans','!=','')->get();
        $documents = Document::where('id_trans','5f3d182947f5')->get(); 
        foreach($documents as $document) {
            $this->info('Traitement en cours : '.$document->id_trans );
           //Check data on LB API
           $transaction = $this->getInfoLB($document->id_trans);
           
    //dd($transaction);
            $document->status = null;
            $document->save();
            $DocumentSellsy = $this->getDocument($document->sellsy);

            //Updage
            $request = array(
                'method' => 'Document.update',
                'params' => Array(
                    'document' => Array(
                        'doctype' => 'invoice'
                    ),
                'docid' => $DocumentSellsy->id,
                ),
            );
            

            //Récupérarion des articles
            $i = 1;
            foreach ($transaction['datas']['produits'] as $item) {
                if($item['nom'] == 'discount') {
                    $request['params']['row'][$i] = array(
                        'row_type' => 'once',
                        'row_name' => $item['nom'],
                        'row_notes' => $item['nom'] . '-' . $transaction['datas']['formulaire'],
                        'row_unit' => 'unité',
                        'row_unitAmount' => 0, //prix unitaire
                        'row_discountUnit' => $item['montant_ht'], //prix unitaire
                        'row_tax' => $transaction['datas']['tva'],
                        'row_qt' => $item['quantite'],
                        'row_isOption' => 'N',
                    );

                } else {
                    $request['params']['row'][$i] = array(
                        'row_type' => 'once',
                        'row_name' => $item['nom'],
                        'row_notes' => $item['nom'] . '-' . $transaction['datas']['formulaire'],
                        'row_unit' => 'unité',
                        'row_unitAmount' => $item['montant_ht'], //prix unitaire
                        'row_tax' => $transaction['datas']['tva'],
                        'row_qt' => $item['quantite'],
                        'row_isOption' => 'N',
                    );

                }
               
                $i++;
                
            } /* End froreach for items */

        

    
        $request['params']['row'][$i] = array('row_type' => 'sum');
        dd($request);
        $response = sellsyConnect::load()->requestApi($request);
        $this->info('Traitement Status  : '.$response->status );
    dd($response);

    } /* End foreach Document */
}
}
