<?php

namespace App\Helpers;
use App\Helpers\sellsyconnect as sellsyConnect;
use DB ;
class sellsyhelpers
{
    
    /**
     * 
     */
    private static   function   issettest( $value ){
        if( isset( $value ) ){
            return $value;
        }
        return null;
    }
    /** 
     * Search client by E-mail address
     */

    //Recherche client par mail
    public static function SearchClientByEmail($email){ 
        
        $request = array(
            'method' => 'Client.getList',
            'params' => array(
                'search' => array(
                    'contains' => $email,
                ),
            ),
        );
        $response = sellsyConnect::load()->requestApi($request);
        $array_client = (array) $response->response->result;
        if($array_client[array_key_first($array_client)]->email == $email) {
            return $client_id = key($array_client);
        }
        return $client_id = null;
            

    }
    public static function updateOrder($idDocument,$id_order)
    {
        DB::table('ps_orders')
            ->where('id_order', $id_order)
            ->update(array('sellsy_document' => $idDocument));
    }
    public static function getOrders ()
    {
        $id_shop = 2;
        $current_state =array(2,3,4,5,9,25,26,27 );
        $orders  = DB::table('ps_orders')
                        ->select('*')
                        ->where('id_shop', $id_shop)
                        ->where('sellsy_document',null)
                        ->whereIn('current_state', $current_state)
                        ->get();

        return $orders ;

    }
    public static function getOrderDetail($id_order)
    {
        $items=DB::table('ps_order_detail')
                    ->select('*')
                    ->where('id_order', $id_order)
                    ->get();
      return $items ;
    }
    /**
     * function paiements
     */
    public static function getPaiements($id_order_ref)
    {
        $paiements = DB::table('ps_order_payment')
                            ->select('*')
                            ->where('order_reference', $id_order_ref)
                            ->where('payment_method','NOT LIKE','%plusieurs%')
                            ->first();
        return $paiements ;
    }
    /**
     * function get method de payment
     */
    public static function getMethodPayment($id_shop,$payment_method)
    {
        $Methodpayment = DB::table('sellsy_paymedium')
                                ->select('*')
                                ->where('id_shop', $id_shop)
                                ->where('name',$payment_method)
                                ->first();
        return $Methodpayment ;
    }
    /**
     * function create  createPayMedium 
     */
    public static function createPayMedium($MethodPayment,$payment_method,$id_shop)
    {
         DB::table('sellsy_paymedium')
                     ->insert(['id'=>$MethodPayment,
                               'name'=>$payment_method,
                               'id_shop'=>$id_shop
                               ]) ;

    }
    /**
     * function update order payement 
     */
    public static function UpdateOrderPayment($id_trans_sellsy,$id_order_ref)
    {
        DB::table('ps_order_payment')
            ->where('order_reference', $id_order_ref)
            ->update(array('sellsy_transaction' => $id_trans_sellsy));
    }
    /**
     * function customer
     */
    public static function getCustomer($id_customer,$id_address_invoice)
    {
        $customer=DB::table('ps_customer')
                            ->select(DB::raw('ps_customer.company AS entreprise, 
                            ps_customer.lastname AS nom,
                            ps_customer.firstname AS prenom,
                                    ps_customer.*, ps_country.*, ps_address.*')
                                )
                            ->join('ps_address', 'ps_address.id_customer', '=', 'ps_customer.id_customer')
                            ->join('ps_country', 'ps_country.id_country', '=', 'ps_address.id_country')
                            ->where('ps_customer.id_customer', $id_customer)
                            ->where('ps_address.id_address', $id_address_invoice)            
                            ->first();
                     
        return $customer ;
    }
   public static function getName($customer)
   {
    if($customer->entreprise != '') 
        return  $customer->entreprise;
    return   $customer->nom . ' ' . $customer->prenom;
    
   }
   public static function getTVA($item)
   {
    $diff = $item->unit_price_tax_incl - $item->unit_price_tax_excl;
    if($diff != 0){
       return  ( ($item->unit_price_tax_incl - $item->unit_price_tax_excl) / $item->unit_price_tax_excl) *100 ;
    }
    return 0 ;
   }
    public static function createClient($customer,$name)
    {
        $request = array(
            'method' => 'Client.create',
            'params' => array(
                'third' => array(
                    'name' => $name ,
                    'email' => $customer->email

                ),
                'contact' => array(
                    'name' => $customer->lastname,
                    'forename' => $customer->firstname,
                    'email' => $customer->email,
                ),
                'address' => array(
                    'name' => "Adresse client",
                    'part1' => $customer->address1,
                    'zip' => $customer->postcode,
                    'town' => $customer->city,
                    'countrycode' => $customer->iso_code,
                ),
            ),
        );
        $response = sellsyConnect::load()->requestApi($request);
            $client_id = $response->response->client_id;
            return $client_id ;
    }
     
    /* public static function createClient( $client, $adresse, $country ){
        

        $request = array(
            'method' => 'Client.create',
            'params' => array(
                'third' => array(
                    'name' => $c->name ,
                    'email' => $c->email ,

                ),
                'contact' => array(
                    'name' => $customer['lastname'],
                    'forename' => $customer['firstname'],
                    'email' => $customer['email'],
                ),
                'address' => array(
                    'name' => "Adresse client",
                    'part1' => $customer['address1'],
                    'zip' => $customer['postcode'],
                    'town' => $customer['city'],
                    'countrycode' => $customer['iso_code'],
                ),
            ),
        );
           //dd($requestClientCreate);
            $response = sellsyConnect::load()->requestApi($requestClientCreate);
           
           
           
            return $clientid = $response->response->client_id;
    } */
    

}


?>