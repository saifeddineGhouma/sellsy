<?php

namespace App\Helpers;
/**
 * LPApi class.
 * 
 * @author   Matthieu Kischel <matthieu@learnybox.com>
 * @link     https://learnybox.com
 */
class LBApi
{
    private $_url;
    private $_api_key;
    private $_datas = array();
    private $_fields_string = '';
    
    public function __construct($url, $api_key)
    {
        $this->_url = $url;
        $this->_api_key = $api_key;       
        $this->_datas['api_key'] = $this->_api_key;
    }
    
    public function set($name, $value = '')
    {
        if (is_array($name)) {
            foreach($name as $id => $value)
                $this->set($id, $value);
        } else {
            $this->_datas[$name] = $value;
        }
        return $this;
    }
    
    private function _getDatas()
    {
        if (!$this->_datas) return;
        
        foreach($this->_datas as $key => $value) {
            $this->_fields_string .= $key.'='.$value.'&';
        }
        $this->_fields_string = rtrim($this->_fields_string, '&');
    }
  
    public function call()
    {
        $this->_getDatas();
        
        if (!isset($this->_datas['action']) or !$this->_datas['action'])
            throw new Exception('Aucune action spécifiée');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_url.$this->_datas['action']);
        curl_setopt($ch, CURLOPT_POST, count($this->_datas));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        if (!$result)
            throw new Exception('Aucun résultat renvoyé par LearnyBox');

        $result = json_decode($result, true);
        if (!$result['status'])
            $this->printError($result);
        
        return $result;
    }
    
    public function printError($call)
    {
        if (isset($call['error']))
            throw new \Exception($call['error']);
        
        if (isset($call['errors'])) {
            $error = '';
            foreach($call['errors'] as $_error)
                $error .= $_error.'<br>';
                
            throw new Exception($error);
        }
    }
}