<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'titre',
		'meta_titre',
		'slug',
		'description',
		'meta_description',
		'template_name',
        'active',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
