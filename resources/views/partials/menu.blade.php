<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="{{ asset('assets/admin/img/profile_small.jpg') }}" />
                    <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                    <span class="text-muted text-xs block">{{ Auth::user()->roles()->first()->title }}</span>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ request()->is('admin') ? 'active' : '' }}">
                <a href="{{ route("admin.home") }}">
                    <i class="fa fa-th-large"></i> <span class="nav-label">{{ trans('global.dashboard') }}</span>
                </a>
            </li>

            <li class="{{ request()->is('admin/learnybox/index*') ? 'active' : '' }} {{ request()->is('admin/ada/index*') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="nav-label">PrestaShop</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ request()->is('admin/ada/index') ? 'active' : '' }}">
                            <a href="{{ route("admin.ada.index") }}">
                                <i class="fa fa-th-large"></i> <span class="nav-label">ADA</span>
                            </a>
                        </li>
                        <li class="{{ request()->is('admin/learnybox/index') ? 'active' : '' }}">
                            <a href="{{ route("admin.learnybox.index") }}">
                                <i class="fa fa-th-large"></i> <span class="nav-label">Learnybox</span>
                            </a>
                        </li>
                    </ul>
                </li>

            @can('user_management_access')
                <li class="{{ request()->is('admin/permissions*') ? 'active' : '' }} {{ request()->is('admin/roles*') ? 'active' : '' }} {{ request()->is('admin/users*') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="nav-label">{{ trans('global.userManagement.title') }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('permission_access')
                            <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.permissions.index") }}">
                                    <i class="fa fa-unlock-alt"></i>
                                    {{ trans('global.permission.title') }}
                                </a>
                            </li>
                        @endcan
                        @can('role_access')
                            <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.roles.index") }}">
                                    <i class="fa fa-briefcase"></i>
                                    {{ trans('global.role.title') }}
                                </a>
                            </li>
                        @endcan
                        @can('user_access')
                            <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.users.index") }}">
                                    <i class="fa fa-user"></i>
                                    {{ trans('global.user.title') }}
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('page_access')
                <li class="{{ request()->is('admin/pages') || request()->is('admin/pages/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.pages.index") }}">
                        <i class="fa fa-file"></i>
                        <span>{{ trans('global.page.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('product_access')
                <li class="{{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.products.index") }}">
                        <i class="fa fa-file-o"></i>
                        <span>{{ trans('global.product.title') }}</span>
                    </a>
                </li>
            @endcan

            {{-- advanced_access --}}
            {{-- @can('settings_management_access') --}}
                <li class="{{ request()->is('admin/laravel-filemanager') || request()->is('admin/laravel-filemanager/*') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span>Advanced</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse {{ request()->is('admin/laravel-filemanager') || request()->is('admin/laravel-filemanager/*') ? 'in' : '' }}">
                        <li class="{{ request()->is('admin/laravel-filemanager') || request()->is('admin/laravel-filemanager/*') ? 'active' : '' }}">
                            <a href="{{ route('admin.unisharp.lfm.show') }}">
                                <i class="fa fa-copy"></i>
                                File manager
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="">
                                <i class="fa fa-terminal"></i>
                                Logs
                            </a>
                        </li>
                    </ul>
                </li>
            {{-- @endcan --}}
            {{-- advanced_access --}}
            @can('settings_management_access')
                <li class="{{ request()->is('admin/settings') || request()->is('admin/settings/*') ? 'active' : '' }}">
                    <a href="#" class="">
                        <i class="fa fa-cog"></i>
                        Settings
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ request()->is('admin/settings') ? 'active' : '' }}">
                            <a href="{{ url('/admin/settings') }}" class="">
                                <i class="fa fa-copy"></i>
                                General
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
        </ul>
    </div>
</nav>