@extends('layouts.app')

@section('content')
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <h2 class="font-bold">{{ trans('global.site_title') }}</h2>
                    <p>
                        {{-- Enter your email address and your password will be reset and emailed to you. --}}
                        {{ trans('global.reset_password') }}
                    </p>
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="m-t" role="form" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                @if(session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="{{ trans('global.login_email') }}" required="" autofocus>
                                    @if($errors->has('email'))
                                        <p class="text-danger">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('global.reset_password') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                SIB Interactive Group
            </div>
            <div class="col-md-6 text-right">
               <small>© {{date('Y')}}</small>
            </div>
        </div>
    </div>
@endsection






{{-- <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Forgot password</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head> --}}