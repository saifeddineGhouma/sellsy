@extends('layouts.app')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div class="ibox-content">
            <div>
                <h1 class="logo-name">
                    <img src="{{ trans('global.logo.url') }}" alt="{{ trans('global.logo.alt') }}">
                </h1>
            </div>
            <h3>Welcome to <br>{{ trans('global.site_title') }}</h3>
            <p>Sign in to start your session</p>
            @if(\Session::has('message'))
                <p class="alert alert-info">
                    {{ \Session::get('message') }}
                </p>
            @endif
            <form class="m-t" role="form" action="{{ route('login') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group @error('email') is-invalid @enderror">
                    <input type="email" class="form-control" placeholder="{{ trans('global.login_email') }}" required="" name="email">
                    @error('email')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="{{ trans('global.login_password') }}" name="password" required="">
                    @error('password')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('global.login') }}</button>

                <a href="{{ route('password.request') }}"><small>{{ trans('global.forgot_password') }}</small></a>
            </form>
            <p class="m-t"> <small>SIB Interactive Group &copy; {{ date('Y') }}</small> </p>
        </div>
    </div>
    {{--      
        <div class="row">
            <div class="col-8">
                <input type="checkbox" name="remember"> {{ trans('global.remember_me') }}
            </div>
        </div>
    --}}
@endsection