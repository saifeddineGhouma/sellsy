@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.create') }} {{ trans('global.user.title_singular') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.users.index") }}">
                        {{ trans('global.user.title_singular') }} {{ trans('global.list') }}
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.create') }} {{ trans('global.user.title_singular') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.create') }} {{ trans('global.user.title_singular') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route("admin.users.store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="name">{{ trans('global.user.fields.name') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($user) ? $user->name : '') }}">
                                    @if($errors->has('name'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('name') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.user.fields.name_helper') }}
                                    </span>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="email">{{ trans('global.user.fields.email') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="email" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}">
                                    @if($errors->has('email'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.user.fields.email_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="password">{{ trans('global.user.fields.password') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="password" id="password" name="password" class="form-control">
                                    @if($errors->has('password'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.user.fields.password_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="roles">{{ trans('global.user.fields.roles') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <select name="roles[]" id="roles" class="form-control select2">
                                        @foreach($roles as $id => $roles)
                                            <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles->contains($id)) ? 'selected' : '' }}>
                                                {{ $roles }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('roles'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('roles') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.user.fields.roles_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a class="btn btn-white btn-sm" href="{{ route("admin.users.index") }}">Cancel</a>
                                    <button class="btn btn-primary btn-sm" type="submit">{{ trans('global.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ asset('assets/admin/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2({
                theme: 'bootstrap4',
            });
        });
    </script>
@endsection