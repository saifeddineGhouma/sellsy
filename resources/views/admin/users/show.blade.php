@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.show') }} {{ trans('global.user.title_singular') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.users.index") }}">
                        {{ trans('global.user.title_singular') }} {{ trans('global.list') }}
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.show') }} {{ trans('global.user.title_singular') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-content text-center">
                    <h1>{{ $user->name }}</h1>
                    <span>{{ $user->email }}</span>
                    <div class="m-b-sm">
                        <img src="{{ asset('assets/admin/img/a4.jpg') }}" class="rounded-circle circle-border m-t-md" alt="profile">
                    </div>
                    <p class="font-bold">
                        @foreach($user->roles as $id => $roles)
                            {{ $roles->title }}
                        @endforeach
                    </p>
                    @if (!is_null($user->email_verified_at))
                        <div>
                            <span>{{ trans('global.user.fields.email_verified_at') }}</span> |
                            <span>{{ $user->email_verified_at }}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>
@endsection