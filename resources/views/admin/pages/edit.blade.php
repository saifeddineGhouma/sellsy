@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.create') }} {{ trans('global.page.title_singular') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.pages.index") }}">
                        {{ trans('global.page.title_singular') }} {{ trans('global.list') }}
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.create') }} {{ trans('global.page.title_singular') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.create') }} {{ trans('global.page.title_singular') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route("admin.pages.update", [$page->id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label>URL</label>
                                <a href="{{ url('/') . '/pages/' . $page->slug }}" target="_blank">{{ url('/') . '/pages/' . $page->slug }}</a>
                            </div>

                            <div class="form-group {{ $errors->has('template_name') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="template_name">{{ trans('global.page.fields.template_name') }}</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="template_name" name="template_name">
                                        <option value="1" @if($page->template_name == 1) selected @endif>Simple colonne</option>
                                        <option value="2" @if($page->template_name == 2) selected @endif>Double colonnes</option>
                                    </select>
                                    @if($errors->has('template_name'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('template_name') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.template_name_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('titre') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="titre">{{ trans('global.page.fields.titre') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="titre" name="titre" class="form-control" value="{{ old('titre', isset($page) ? $page->titre : '') }}" required>
                                    @if($errors->has('titre'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('titre') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.titre_helper') }}
                                    </span>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="slug">{{ trans('global.page.fields.slug') }}</label>
                                <div class="col-sm-10">
                                    <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', isset($page) ? $page->slug : '') }}">
                                    @if($errors->has('slug'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('slug') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.slug_helper') }}
                                    </span>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>
                            
                            <h3 class="m-x">{{ trans('global.page.fields.metas') }}</h3>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('meta_titre') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="meta_titre">{{ trans('global.page.fields.meta_titre') }}</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="meta_titre" name="meta_titre" value="{{ old('meta_titre', isset($page) ? $page->meta_titre : '') }}" required>
                                    @if($errors->has('meta_titre'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('meta_titre') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.meta_titre_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="meta_description">{{ trans('global.page.fields.meta_description') }}</label>
                                <div class="col-sm-10">
                                    <textarea id='meta_description' class="form-control" rows="5" id="meta_description" name="meta_description">{{ old('meta_description', isset($page) ? $page->meta_description : '') }}</textarea>
                                    @if($errors->has('meta_description'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('meta_description') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.meta_description_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            
                            <h3 class="m-x">{{ trans('global.page.fields.description.name') }}</h3>

                            <div class="hr-line-dashed"></div>
                            
                            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="description">{{ trans('global.page.fields.description.label') }}</label>
                                <div class="col-sm-10">
                                    <textarea id='description' class="form-control summernote" rows="5" id="description" name="description">{{ old('description', isset($page) ? $page->description : '') }}</textarea>
                                    @if($errors->has('description'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('description') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.description_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            
                            <div class="form-group {{ $errors->has('active') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="active">{{ trans('global.page.fields.active.name') }}</label>
                                <div class="col-sm-10">
                                    <div class="i-checks">
                                        <label>
                                            <input type="checkbox" class="i-checks" @if($page->active == 1) checked @endif name="active">
                                            <i></i>
                                            {{ trans('global.page.fields.active.label') }}
                                        </label>
                                    </div>

                                    @if($errors->has('active'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('active') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.page.fields.active_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a class="btn btn-white btn-sm" href="{{ route("admin.pages.index") }}">Cancel</a>
                                    <button class="btn btn-primary btn-sm" type="submit">{{ trans('global.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- iCheck -->
    <script src="{{ asset('assets/admin/js/plugins/iCheck/icheck.min.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ asset('assets/admin/js/plugins/select2/select2.full.min.js') }}"></script>

    <!-- SUMMERNOTE -->
    <script src="{{ asset('assets/admin/js/plugins/summernote/summernote-bs4.js') }}"></script>

    <script>
        $(document).ready(function(){
            $(".select2").select2({
                theme: 'bootstrap4',
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.summernote').summernote({
                height: 600
            });
        });
    </script>
@endsection