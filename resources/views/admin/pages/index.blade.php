@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.page.title_singular') }} {{ trans('global.list') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.page.title_singular') }} {{ trans('global.list') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            @can('page_create')
                <div class="title-action">
                    <a class="btn btn-success" href="{{ route("admin.pages.create") }}">
                        {{ trans('global.add') }} {{ trans('global.page.title_singular') }}
                    </a>
                </div>
            @endcan
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.page.title_singular') }} {{ trans('global.list') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        {{-- <th width="10">
                                
                                        </th> --}}
                                        <th>
                                            {{ trans('global.page.fields.titre') }}
                                        </th>
                                        <th>
                                            {{ trans('global.page.fields.slug') }}
                                        </th>
                                        <th>
                                            {{ trans('global.page.fields.template_name') }}
                                        </th>
                                        <th>
                                            {{ trans('global.page.fields.active.label') }}
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        {{-- <th width="10">
                                
                                        </th> --}}
                                        <th>
                                            {{ trans('global.page.fields.titre') }}
                                        </th>
                                        <th>
                                            {{ trans('global.page.fields.slug') }}
                                        </th>
                                        <th>
                                            {{ trans('global.page.fields.template_name') }}
                                        </th>
                                        <th>
                                            {{ trans('global.page.fields.active.label') }}
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script src="{{ asset('assets/admin/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {
                        extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
                ajax: '{{ route("admin.pages.getData") }}',
                order: [[ 1, 'asc' ]],
                columns: [
                    { data: 'titre', name: 'titre', searchable: true, orderable: true },
                    { data: 'slug', name: 'slug', searchable: true, orderable: true },
                    { data: 'template_name', name: 'template_name', searchable: true, orderable: true },
                    { data: 'active', name: 'active', searchable: true, orderable: true },
                    { data: 'actions', name: 'actions', searchable: false, orderable: false},
                ]
            });
        });

        function ConfirmDelete()
        {
            var x = confirm(@json(trans('global.areYouSure')));
            if (x)
                return true;
            else
                return false;
        }
    </script>
@endsection