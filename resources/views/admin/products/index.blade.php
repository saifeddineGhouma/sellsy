@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">    
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.product.title_singular') }} {{ trans('global.list') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.product.title_singular') }} {{ trans('global.list') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            @can('product_create')
                <div class="title-action">
                    <a class="btn btn-success" href="{{ route("admin.products.create") }}">
                        {{ trans('global.add') }} {{ trans('global.product.title_singular') }}
                    </a>
                </div>
            @endcan
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.product.title_singular') }} {{ trans('global.list') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th width="10">

                                        </th>
                                        <th>
                                            {{ trans('global.product.fields.name') }}
                                        </th>
                                        <th>
                                            {{ trans('global.product.fields.description') }}
                                        </th>
                                        <th>
                                            {{ trans('global.product.fields.price') }}
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $key => $product)
                                        <tr class="gradeX" data-entry-id="{{ $product->id }}">
                                            <td>

                                            </td>
                                            <td>
                                                {{ $product->name ?? '' }}
                                            </td>
                                            <td>
                                                {{ $product->description ?? '' }}
                                            </td>
                                            <td>
                                                {{ $product->price ?? '' }}
                                            </td>
                                            <td>
                                                @can('product_show')
                                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.products.show', $product->id) }}" title="{{ trans('global.view') }}">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
                                                @can('product_edit')
                                                    <a class="btn btn-xs btn-info" href="{{ route('admin.products.edit', $product->id) }}" title="{{ trans('global.edit') }}">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                @endcan
                                                @can('product_delete')
                                                    <form action="{{ route('admin.products.destroy', $product->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" class="btn btn-xs btn-danger" title="{{ trans('global.delete') }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                    </form>
                                                @endcan
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="10">

                                        </th>
                                        <th>
                                            {{ trans('global.product.fields.name') }}
                                        </th>
                                        <th>
                                            {{ trans('global.product.fields.description') }}
                                        </th>
                                        <th>
                                            {{ trans('global.product.fields.price') }}
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

    <script src="{{ asset('assets/admin/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {
                        extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });
    </script>
@endsection

{{-- <script>
    $(function () {
        let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        let deleteButton = {
            text: deleteButtonTrans,
            url: "{{ route('admin.pages.massDestroy') }}",
            className: 'btn-danger',
            action: function (e, dt, node, config) {
            var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                return $(entry).data('entry-id')
            });

            if (ids.length === 0) {
                alert('{{ trans('global.datatables.zero_selected') }}')

                return
            }

            if (confirm('{{ trans('global.areYouSure') }}')) {
                $.ajax({
                headers: {'x-csrf-token': _token},
                method: 'POST',
                url: config.url,
                data: { ids: ids, _method: 'DELETE' }})
                .done(function () { location.reload() })
            }
            }
        }
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @can('page_delete')
            dtButtons.push(deleteButton)
        @endcan
        $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    })
</script> --}}