@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.create') }} {{ trans('global.product.title_singular') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.products.index") }}">
                        {{ trans('global.product.title_singular') }} {{ trans('global.list') }}
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.create') }} {{ trans('global.product.title_singular') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.create') }} {{ trans('global.product.title_singular') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route("admin.products.store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="name">{{ trans('global.product.fields.name') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($product) ? $product->name : '') }}">
                                    @if($errors->has('name'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('name') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.product.fields.name_helper') }}
                                    </span>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="description">{{ trans('global.product.fields.description') }}</label>
                                <div class="col-sm-10">
                                    <textarea id="description" name="description" class="form-control ">{{ old('description', isset($product) ? $product->description : '') }}</textarea>
                                    @if($errors->has('description'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('description') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.product.fields.description_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="price">{{ trans('global.product.fields.price') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="number" id="price" name="price" class="form-control" value="{{ old('price', isset($product) ? $product->price : '') }}" step="0.01">
                                    @if($errors->has('price'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('price') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.product.fields.price_helper') }}
                                    </span>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a class="btn btn-white btn-sm" href="{{ route("admin.products.index") }}">Cancel</a>
                                    <button class="btn btn-primary btn-sm" type="submit">{{ trans('global.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ asset('assets/admin/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2({
                theme: 'bootstrap4',
            });
        });
    </script>
@endsection