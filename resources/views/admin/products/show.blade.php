@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.show') }} {{ trans('global.product.title_singular') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.permissions.index") }}">
                        {{ trans('global.permission.title_singular') }} {{ trans('global.list') }}
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.show') }} {{ trans('global.product.title_singular') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.show') }} {{ trans('global.product.title_singular') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="border-top-0">
                                        <strong>
                                            {{ trans('global.product.fields.name') }}
                                        </strong>
                                    </td>
                                    <td class="border-top-0">
                                        {{ $product->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            {{ trans('global.product.fields.description') }}
                                        </strong>
                                    </td>
                                    <td>
                                        {!! $product->description !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            {{ trans('global.product.fields.price') }}
                                        </strong>
                                    </td>
                                    <td>
                                        ${{ $product->price }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>
@endsection