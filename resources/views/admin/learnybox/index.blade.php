@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Learnybox</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Learnybox</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Learnybox</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>
                                            LearnyBox Trans. ID
                                        </th>
                                        <th>
                                            Nom
                                        </th>
                                        <th>
                                            Prenom
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Sellsy Doc ID
                                        </th>
                                        <th>
                                           Montant LB
                                        </th>
                                        <th>
                                            Montant Sellsy
                                        </th>
                                        <th>
                                            Statut
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>
                                            LearnyBox Trans. ID
                                        </th>
                                        <th>
                                            Nom
                                        </th>
                                        <th>
                                            Prenom
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Sellsy Doc ID
                                        </th>
                                        <th>
                                           Montant LB
                                        </th>
                                        <th>
                                            Montant Sellsy
                                        </th>
                                        <th>
                                            Statut
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script src="{{ asset('assets/admin/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Page-Level Scripts -->
    <script>
        var table = null
        $(document).ready(function(){
            table = $('.dataTables-example').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {
                        extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
                ajax: '{{ route("admin.learnybox.getData") }}',
                order: [[ 1, 'asc' ]],
                columns: [
                    { data: 'id_trans', name: 'id_trans', searchable: true, orderable: true },
                    { data: 'nom', name: 'nom', searchable: true, orderable: true },
                    { data: 'prenom', name: 'prenom', searchable: true, orderable: true },
                    { data: 'email', name: 'email', searchable: true, orderable: true },
                    { data: 'sellsy', name: 'sellsy', searchable: true, orderable: true },
                    { data: 'mnt_lb', name: 'mnt_lb', searchable: true, orderable: true },
                    { data: 'mnt_sellsy', name: 'mnt_sellsy', searchable: true, orderable: true },
                    { data: 'status', name: 'status', searchable: true, orderable: true },
                ]
            });
        });

        function ConfirmDelete()
        {
            var x = confirm(@json(trans('global.areYouSure')));
            if (x)
                return true;
            else
                return false;
        }

        function valider(url)
        {
            var x = confirm(@json(trans('global.areYouSure')));
            if (x)
            {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {},
                    success: success,
                    dataType: "json",
                    async:false
                });
                return false;
            }
            else
                return false;
        }

        function success(data)
        {
            table.ajax.reload( null, false );
        }
    </script>
@endsection
