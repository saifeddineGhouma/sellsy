@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('assets/admin/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/plugins/select2/select2-bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ trans('global.create') }} {{ trans('global.role.title_singular') }}</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.home") }}">{{ trans('global.dashboard') }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route("admin.roles.index") }}">
                        {{ trans('global.role.title_singular') }} {{ trans('global.list') }}
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>{{ trans('global.create') }} {{ trans('global.role.title_singular') }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ trans('global.create') }} {{ trans('global.role.title_singular') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route("admin.roles.store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="title">{{ trans('global.role.fields.title') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($role) ? $role->title : '') }}">
                                    @if($errors->has('title'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('title') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.role.fields.title_helper') }}
                                    </span>
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }} row">
                                <label class="col-sm-2 col-form-label" for="permissions">{{ trans('global.role.fields.permissions') }} <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <select name="permissions[]" id="permissions" class="form-control select2" multiple="multiple">
                                        @foreach($permissions as $id => $permissions)
                                            <option value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'selected' : '' }}>
                                                {{ $permissions }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('permissions'))
                                        <span class="form-text m-b-none text-danger">
                                            {{ $errors->first('permissions') }}
                                        </span>
                                    @endif
                                    <span class="form-text m-b-none">
                                        {{ trans('global.role.fields.permissions_helper') }}
                                    </span>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <a class="btn btn-white btn-sm" href="{{ route("admin.roles.index") }}">Cancel</a>
                                    <button class="btn btn-primary btn-sm" type="submit">{{ trans('global.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('assets/admin/js/inspinia.js') }}"></script>
    <script src="{{ asset('assets/admin/js/plugins/pace/pace.min.js') }}"></script>

    <!-- Select2 -->
    <script src="{{ asset('assets/admin/js/plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2({
                theme: 'bootstrap4',
            });
        });
    </script>
@endsection