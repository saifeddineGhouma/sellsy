<div class="{{ Arr::get($fields, 'section_class', config('app_settings.section_class', 'card')) }} section-{{ Str::slug($fields['title']) }}">
    <div class="{{ Arr::get($fields, 'section_heading_class', config('app_settings.section_heading_class', 'card-header')) }}">
        {{-- <i class="{{ Arr::get($fields, 'icon', 'glyphicon glyphicon-flash') }}"></i> --}}
        <h5>{{ $fields['title'] }} @if( $desc = Arr::get($fields, 'descriptions') ) <small class="text-muted mb-0 "> {{ $desc }}</small>@endif</h5>
    </div>
    {{ $slot }}
</div>
<!-- end card for {{ $fields['title'] }} -->
