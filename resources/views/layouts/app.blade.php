<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ trans('global.site_title') }}</title>

        <link href="{{ asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/admin/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet">
    </head>

    <body class="gray-bg">

        @yield('content')

        <!-- Mainly scripts -->
        <script src="{{ asset('assets/admin/js/jquery-2.1.1.js') }}"></script>
        <script src="{{ asset('assets/admin/js/bootstrap.min.js') }}"></script>
    </body>
</html>
