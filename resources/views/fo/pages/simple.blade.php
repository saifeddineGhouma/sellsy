@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col">
			<h1 style="margin: 0;">{{ $page->titre }}</h1>
			<p>
				{!! $page->description !!}
			</p>
		</div>
	</div>
@endsection
