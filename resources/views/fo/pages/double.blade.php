@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-9">
			<h1 style="margin: 0;">{{ $page->titre }}</h1>
			<p>
				{!! $page->description !!}
			</p>
		</div>
		<div class="col-3">
			<h2 style="margin: 0;">Right block</h2>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
		</div>
	</div>
@endsection
