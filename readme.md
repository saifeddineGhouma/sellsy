# Clean Laravel Project

## What's inside

- Adminpanel with default one admin user (_dev@sib-interactive.com/dev@sib-interactive.com_) and two roles
- Users/Roles/permissions management function based on Spatie Roles-Permissions.
- One demo CRUD for Products management - name/description/price
- Everything that is needed for CRUDs: model+migration+controller+requests+views
- .env editing interface using [GeoSot/Laravel-EnvEditor](https://github.com/GeoSot/Laravel-EnvEditor).



## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (Some seeded data for testing)
- That's it: launch the main URL or go to __/login__ and login with default credentials __dev@sib-interactive.com__ - __dev@sib-interactive.com__